import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class GreenPaint extends CustomPainter {

  Color color;

  GreenPaint({this.color});

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = color;
    paint.strokeWidth = 20;

    //draw one line
//    canvas.drawLine(
//        Offset(225.0, 170.0),
//        Offset(123.0, 170.0),
//        paint
//    );

    //draw two lines
    canvas.drawLine(
        Offset(140.0, 255.0),
        Offset(90.0, 255.0),
        paint);

    canvas.drawLine(
        Offset(280, 278),
        Offset(180, 278),
        paint
    );

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }

}