import 'package:flutter/material.dart';
import 'package:quran_pages/Painter.dart';

void main() => runApp(MaterialApp(
      home: QuranPages(),
      debugShowCheckedModeBanner: false,
      theme:
          ThemeData(primaryIconTheme: IconThemeData(color: Colors.teal[600])),
    ));

class QuranPages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    bool isLargeScreen = false;

    if (MediaQuery.of(context).size.width > 530) {
      isLargeScreen = true;
    } else {
      isLargeScreen = false;
    }

    return isLargeScreen ? QuranPagesTablet() : QuranPagesMobile();
  }
}


  _greenContainer(
      {@required double height, @required double width, @required Color color}) {
    return Container(
      height: height,
      width: width,
      color: color,
    );
  }

//For Mobile Layout
class QuranPagesMobile extends StatefulWidget {
  @override
  _QuranPagesMobileState createState() => _QuranPagesMobileState();
}

class _QuranPagesMobileState extends State<QuranPagesMobile> {

  bool onPressed = false;

  GlobalKey _greenKey = GlobalKey();

  _getPositions() {
    final RenderBox renderBoxRed = _greenKey.currentContext.findRenderObject();
    final positionRed = renderBoxRed.localToGlobal(Offset.zero);
    print("POSITION of Red: $positionRed ");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: Container(width: 60, child: Drawer()),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        flexibleSpace: Stack(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(width: 4),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      Icons.note_add,
                      color: Colors.teal[600],
                    ),
                    Text("فاصل جديد"),
                    SizedBox(height: 4)
                  ],
                ),
                SizedBox(
                  width: 1,
                ),
                Container(
                    margin: EdgeInsets.only(top: 23, right: 8),
                    height: 80,
                    width: 0.5,
                    color: Colors.black87),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      Icons.search,
                      color: Colors.teal[600],
                    ),
                    Text("الفواصل"),
                    SizedBox(height: 4)
                  ],
                ),
                SizedBox(
                  width: 5,
                ),
                Container(
                    margin: EdgeInsets.only(top: 23),
                    height: 80,
                    width: 0.5,
                    color: Colors.black87),
                SizedBox(width: 85),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 25,
                      ),
                      child: Icon(
                        Icons.keyboard_arrow_down,
                        size: 28,
                        color: Colors.teal[600],
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.only(top: 10, right: 42),
                        child: Text(
                          "الفاتحة",
                          style: TextStyle(fontSize: 18),
                        )),
                  ],
                ),
              ],
            ),
            Positioned(
              right: 50,
              bottom: 2,
              child: Text(
                "الآيه ١",
                style: TextStyle(fontSize: 17),
              ),
            )
          ],
        ),
      ),
      body: Column(
        children: <Widget>[

          Stack(
            children: <Widget>[

              Container(
                  height: MediaQuery.of(context).size.height < 540
                      ? 345
                      : MediaQuery.of(context).size.height - 180,
                  width: MediaQuery.of(context).size.width,

                  //Page 1
                  child: Image.asset(
                    'assets/images/1.png',
                    filterQuality: FilterQuality.high,
                    fit: BoxFit.cover,
                  )),
              Positioned(
                left: 1,
                top: 80,
                child: Container(
                    height: 45,
                    width: 40,
                    color: Colors.white,
                    child: Icon(Icons.content_copy,
                        color: Colors.teal[600], size: 27)),
              ),

              Container(
                child: CustomPaint(
                  painter: GreenPaint(color: Colors.green.withOpacity(0.4)),
                ),
              )

              //Old Green Container آيه 1
//              Positioned(
//                right: 110,
//                top: 175,
//                child: Container(
//                  key: _greenKey,
//                  child: InkWell(
//                    onLongPress: () => setState(() => onPressed = !onPressed),
//                    child: _greenContainer(
//                      height: 19,
//                      width: 120,
//                      color: onPressed
//                          ? Colors.green.withOpacity(0.4)
//                          : Colors.transparent,
//                    ),
//                  ),
//                ),
//              ),
            ],
          ),


          SizedBox(height: 8),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(width: 1),

              //القارئ
              Container(
                height: 35,
                width: 90,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.blueGrey[500]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width: 3),
                    Text(
                      "الحذيفي",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Icon(Icons.person, color: Colors.white)
                  ],
                ),
              ),

              Row(
                children: <Widget>[
                  //Search Icon
                  CircleAvatar(
                      child: Icon(Icons.search, color: Colors.white),
                      maxRadius: 17,
                      backgroundColor: Colors.teal[600]),

                  SizedBox(width: 7),

                  //Play Icon
                  InkWell(
                    onTap: _getPositions,
                    child: CircleAvatar(
                        child: Icon(Icons.play_arrow, color: Colors.white),
                        maxRadius: 17,
                        backgroundColor: Colors.teal[600]),
                  ),

                  SizedBox(width: 7),

                  //Repeat Icon
                  CircleAvatar(
                      child: Icon(Icons.autorenew, color: Colors.white),
                      maxRadius: 17,
                      backgroundColor: Colors.teal[600]),

                  SizedBox(width: 7),
                ],
              ),

              //التفسير
              Container(
                height: 35,
                width: 90,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.blueGrey[500]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width: 6),
                    Text(
                      "التفسير",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Icon(Icons.edit, color: Colors.white)
                  ],
                ),
              ),
              SizedBox(width: 1)
            ],
          ),
        ],
      ),
      bottomNavigationBar: Container(
        height: 50,
        color: Colors.grey[300].withAlpha(150),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(width: 1),

            //خطأ حفظ
            Row(
              children: <Widget>[
                Text(
                  "خطأ حفظ",
                  style: TextStyle(color: Colors.blueGrey, fontSize: 17),
                ),
                SizedBox(width: 6),
                Icon(Icons.radio_button_unchecked,
                    color: Colors.orange[800], size: 17),
              ],
            ),
            //خطأ تجويد
            Row(
              children: <Widget>[
                Text(
                  "خطأ تجويد",
                  style: TextStyle(color: Colors.blueGrey, fontSize: 17),
                ),
                SizedBox(width: 6),
                Icon(Icons.radio_button_unchecked,
                    color: Colors.yellow[800], size: 17),
              ],
            ),

            //تعليق
            Row(
              children: <Widget>[
                Text(
                  "تعليق",
                  style: TextStyle(color: Colors.blueGrey, fontSize: 17),
                ),
                SizedBox(width: 6),
                Icon(Icons.radio_button_unchecked,
                    color: Colors.blue[800], size: 17),
              ],
            ),

            SizedBox(width: 1),
          ],
        ),
      ),
    );
  }
}





//For Tablet Layout
class QuranPagesTablet extends StatefulWidget {
  @override
  _QuranPagesTabletState createState() => _QuranPagesTabletState();
}

class _QuranPagesTabletState extends State<QuranPagesTablet> {
  bool onPressed = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: Container(width: 150, child: Drawer()),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        flexibleSpace: Stack(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(width: 4),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      Icons.note_add,
                      color: Colors.teal[600],
                    ),
                    Text("فاصل جديد"),
                  ],
                ),
                Container(
                    margin: EdgeInsets.only(top: 24, right: 8),
                    height: 80,
                    width: 0.5,
                    color: Colors.black87),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      Icons.search,
                      color: Colors.teal[600],
                    ),
                    Text("الفواصل"),
                  ],
                ),
                Container(
                    margin: EdgeInsets.only(top: 24),
                    height: 80,
                    width: 0.5,
                    color: Colors.black87),
                SizedBox(width: 120),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 25,
                      ),
                      child: Icon(
                        Icons.keyboard_arrow_down,
                        size: 28,
                        color: Colors.teal[600],
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.only(top: 10, right: 42),
                        child: Text(
                          "الفاتحة",
                          style: TextStyle(fontSize: 18),
                        )),
                  ],
                ),
//                SizedBox(width: 35)
              ],
            ),
            Positioned(
              right: 50,
              bottom: - 3,
              child: Text(
                "الآيه ١",
                style: TextStyle(fontSize: 17),
              ),
            )
          ],
        ),
      ),
      body: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                  height: 600,//385
                  width: MediaQuery.of(context).size.width,
                  //Page 1
                  child: Image.asset(
                    'assets/images/1.png',
                    filterQuality: FilterQuality.high,
                    fit: BoxFit.fitWidth,
                  )),
              Positioned(
                left: 1,
                top: 80,
                child: Container(
                    height: 45,
                    width: 40,
                    color: Colors.white,
                    child: Icon(Icons.content_copy,
                        color: Colors.teal[600], size: 27)),
              ),

              //Green Container آيه 1
              Positioned(
                left:
                    202,
                top: 247,
                child: InkWell(
                  onLongPress: () => setState(() => onPressed = !onPressed),
                  child: _greenContainer(
                    height: 21,
                    width: 133,
                    color: onPressed
                        ? Colors.green.withOpacity(0.4)
                        : Colors.transparent,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(width: 1),

              //القارئ
              Container(
                height: 40,
                width: 90,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.blueGrey[500]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "الحذيفي",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Icon(Icons.person, color: Colors.white)
                  ],
                ),
              ),

              Row(
                children: <Widget>[
                  //Search Icon
                  CircleAvatar(
                      child: Icon(Icons.search, color: Colors.white),
                      maxRadius: 20,
                      backgroundColor: Colors.teal[600]),

                  SizedBox(width: 10),

                  //Play Icon
                  CircleAvatar(
                      child: Icon(Icons.play_arrow, color: Colors.white),
                      maxRadius: 20,
                      backgroundColor: Colors.teal[600]),

                  SizedBox(width: 10),

                  //Repeat Icon
                  CircleAvatar(
                      child: Icon(Icons.autorenew, color: Colors.white),
                      maxRadius: 20,
                      backgroundColor: Colors.teal[600]),

                  SizedBox(width: 10),
                ],
              ),

              //التفسير
              Container(
                height: 40,
                width: 90,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.blueGrey[500]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "التفسير",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Icon(Icons.edit, color: Colors.white)
                  ],
                ),
              ),
              SizedBox(width: 1)
            ],
          ),
//          SizedBox(height: 13),
        ],
      ),
      bottomNavigationBar: Container(
        height: 50,
        color: Colors.grey[300].withAlpha(150),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(width: 1),

            //خطأ حفظ
            Row(
              children: <Widget>[
                Text(
                  "خطأ حفظ",
                  style: TextStyle(color: Colors.blueGrey, fontSize: 17),
                ),
                SizedBox(width: 6),
                Icon(Icons.radio_button_unchecked,
                    color: Colors.orange[800], size: 17),
              ],
            ),
            //خطأ تجويد
            Row(
              children: <Widget>[
                Text(
                  "خطأ تجويد",
                  style: TextStyle(color: Colors.blueGrey, fontSize: 17),
                ),
                SizedBox(width: 6),
                Icon(Icons.radio_button_unchecked,
                    color: Colors.yellow[800], size: 17),
              ],
            ),

            //تعليق
            Row(
              children: <Widget>[
                Text(
                  "تعليق",
                  style: TextStyle(color: Colors.blueGrey, fontSize: 17),
                ),
                SizedBox(width: 6),
                Icon(Icons.radio_button_unchecked,
                    color: Colors.blue[800], size: 17),
              ],
            ),

            SizedBox(width: 1),
          ],
        ),
      ),
    );
  }
}

